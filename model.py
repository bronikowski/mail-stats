# encoding: utf8
from sqlobject import *

class MailItem(SQLObject):
    h_from = StringCol()
    h_subject = StringCol()
    h_date = DateTimeCol()
